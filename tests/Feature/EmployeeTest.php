<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeTest extends TestCase
{
    //this trait helps us to remove all the tables and migrate fresh database on every run
    use RefreshDatabase;

    /*
    * Boot the application tests with seeders
    */
    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }

    /**
     * A test case to get a list of employees
     *
     * @return void
    */
    public function testBasicTest()
    {
        $response = $this->get('/api/employee');
        $response->assertStatus(200);
    }
}
