
## About Project

It is a demo project that shows you how to write test cases using SQLite3 database in laravel framework:

## Steps to install this project
1. First install laravel framework, if you don't know how to install it refer to this [link](https://laravel.com/docs/6.x/installation)
2. Integration of sqlite3
    - ```sudo apt-get install php{PHP_VERSION}-sqlite3``` (in my case PHP_VERSION=7.2)
3. Setup env for sqlite3 database: 
    - In laravel version 6.x we have pre-configured phpunit.xml file in which two variables are set up, if not set you can set it up manually like:
        - ```<server name="DB_CONNECTION" value="sqlite"/>```
        - ```<server name="DB_DATABASE" value=":memory:"/>```

# How to run this project
- On your terminal, root of this project run the below command:
    - ./vendor/bin/phpunit
